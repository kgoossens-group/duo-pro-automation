// gitlab graphql request to enable duo pro

        // define the gitlab graphql client
        $client = new Client(
            config('demosys.omnibus.'. $account_invitation->type. '.base_url') . '/api/graphql',
            ['Authorization' => 'Bearer ' . config('demosys.omnibus.'. $account_invitation->type. '.api_key')]
        );

        $mutation = (new Mutation('userAddOnAssignmentCreate'))
            ->setArguments([
                'input' => new RawObject('{userId: "gid://gitlab/User/' . $gitlab_user['id'] . '", addOnPurchaseId: "gid://gitlab/GitlabSubscriptions::AddOnPurchase/{license id removed}", clientMutationId: null}')
            ])
            ->setSelectionSet([
                'errors',
                'clientMutationId',
                (new Query('addOnPurchase'))->setSelectionSet([
                    'id',
                    'name',
                    'purchasedQuantity',
                    'assignedQuantity',
                ]),
                (new Query('user'))->setSelectionSet([
                    'id',
                    (new Query('addOnAssignments'))
                        ->setArguments(['addOnPurchaseIds' => "gid://gitlab/GitlabSubscriptions::AddOnPurchase/{license id removed}"])
                        ->setSelectionSet([
                            (new Query('nodes')) ->setSelectionSet([
                                (new Query('addOnPurchase')) ->setSelectionSet([
                                    'name',
                                ])
                            ])
                        ]),
                ]),
            ]);

        $client->runQuery($mutation, true);

        return (object) [
            'account_invitation_id' => $account_invitation->id,
            'account_user_id' => $account_user->id,
            'gitlab_url' => config('demosys.omnibus.training.base_url'),
            'gitlab_group_url' => $gitlab_child_group['web_url'],
            'username' => $generated_username,
            'password' => $generated_password,
            'invitation_type' => 'normal'
        ];